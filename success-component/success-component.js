{
  const {
    html,
  } = Polymer;
  /**
    `<success-component>` Description.

    Example:

    ```html
    <success-component></success-component>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --success-component | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class SuccessComponent extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior, ], Polymer.Element) {
    static get is() {
      return 'success-component';
    }

    static get properties() {
      return {
       exito:{
         type:Object,
         value: 
           {
            header: [
              {
                key: {
                text: "Nueva línea de crédito",
                class: "title"
                },
                value: dateFormated(new Date()),
                class: "spacing-lg",
              },
              {
                value: objectAmount(35000.00,'amount-huge-decimals'),
                class: "spacing-lg-bottom",
              },
            ],
            middle: [
              {
                image: {
                  text: './resources/illustrations/account-card.svg',
                  class: 'card',
                },
                class: 'rounded',
              },
            ],
            main: [
              {
                key: '',
                value: [
                  {
                    text: 'Tarjeta de credito azul',
                    class: 'title',
                  },
                  {
                    text: '1212312331234',
                    masked: true,
                    maskChars: '•',
                    visibleChars: 4,
                    class: 'italic',
                  },
                ],
                class: 'spacing-top spacing-lg-bottom',
              },
              {
                key: 'Folio: 23412342',
                value: {
                  text: '',
                  class: 'medium',
                },
                class: 'spacing-lg-bottom',
              },
              {
                key: 'Recibirás un SMS informativo y el comprobante de la operacion al correo',
                value: {
                  text: 'rosas@gmail.com',
                  masked: true,
                  maskChars: '•',
                  visibleChars: 15,
                  class: 'italic medium spacing-sm-top',
                },
              },
            ],
            footer: [
              {
                button: {
                  text: 'Salir',
                  class: 'primary',
                },
                event: 'next2',
                class: 'spacing-lg',
              },
            ]
           }
       }
      };
    }

    ready(){
      super.ready();
    }
  }

  const dateFormated = (date) => ({
    text: date,
    formatDate: 'D MMMM YYYY, HH:mm [h]',
  });

  const objectAmount = (amount, className = '', currencyCode = '') => ({
    amount,
    currencyCode,
    localCurrency: 'PEN',
    language: 'es-pe',
    class: className,
  });

  customElements.define(SuccessComponent.is, SuccessComponent);
}